#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define SEED 123

void free_matrix(int **m, int size) {
    for (int i = 0; i < size; i++)
        free(m[i]);
    free(m);
}

int **mul(int **a, int **b, int size) {
    int **ret = malloc(size * sizeof(int *));
    for (int i = 0; i < size; i++) {
        ret[i] = calloc(size, sizeof(int));
        for (int j = 0; j < size; j++)
            for (int k = 0; k < size; k++)
                ret[i][j] += a[i][k] * b[k][j];
    }
    return ret;
}

int logaritmo(int n){ /* It returns the log2(n) */
    int i = 1, count = 0;
    while (i < n){
        i= i * 2;
        count++;
    }
    return count;
}

int multiplywith(int i){  /*Calculates 2^(i-1)*/
    int output = 1;
    i--;
    while (i > 0){
        output = output * 2;
        i--;
    }
    return output;
}

// Parallelise this function:
void array_mul(int ***data, int n, int size) {
    int height = logaritmo(n), i = 0, mult = 2, j = 0;
    int check[height][n];
#   pragma omp parallel default(none) \
    shared(height,i,j,mult,check,size,n,data) 
    {
#       pragma omp single nowait
        {
            for(i=1;i<=height;i++,mult*=2){
                for(j=0;j<n;j+=mult){
                    if (i == 0){
                        int ib = i, jb = j, with; 
#                       pragma omp task depend(out: check[0][jb]) \
                        firstprivate(ib,jb) private(with)
                        {
                            with = multiplywith(ib) + jb;
                            if (with < n)
                                data[jb] = mul(data[jb], data[with], size);
                            
                        } /*end omp task*/
                    }/*endif*/
                    else{
                        int ib = i, jb = j, with = multiplywith(ib) + jb;
                        if (with < n){
#                           pragma omp task depend(in: check[ib-1][jb], check[ib-1][with]) \
                            depend(out: check[ib][jb]) firstprivate(ib,jb,with)
                            {
                                data[jb] = mul(data[jb],data[with],size);
                            }
                        }
                        else{
                            int lixo = 2;
#                           pragma omp task depend(in: check[ib-1][jb]) \
                            depend(out: check[ib][jb]) firstprivate(ib,lixo)
                            {
                                lixo++;
                            }                                  
                        }
                    }/*endelse*/
                }
            }
        } /*end omp single*/
    } /*end omp parallel*/
}

int **rnd_matrix(int size) {
    int **ret = malloc(size * sizeof(int *));
    for (int i = 0; i < size; i++) {
        ret[i] = malloc(size * sizeof(int));
        for (int j = 0; j < size; j++)
            ret[i][j] = 2 * (rand() % 2) - 1; // Generates -1 or 1
    }

    return ret;
}

void print_matrix(int **m, int size) {
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++)
            printf("%d ", m[i][j]);
        printf("\n");
    }
}

int main() {
    int n, size;
    scanf("%d %d", &n, &size);

    srand(SEED);

    int ***data = malloc(n * sizeof(int **));
    for (int i = 0; i < n; i++)
        data[i] = rnd_matrix(size);
    double t_start = omp_get_wtime();
    array_mul(data, n, size);
    double t_stop = omp_get_wtime();
    print_matrix(data[0], size);
    printf("Time spent: %lf\n", t_stop - t_start);
    free(data);
    return 0;
}
